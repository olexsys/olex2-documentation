---
title: "The Log File"
linkTitle: "Log"
date: 2021-03-01
weight: 10
description: About the Olex2 log file.
categories:
 - Debugging
tags:
 - Version
 - Log
 - Crash
---

The Log file contains a lot of information about your Olex2 session. When problems occur, getting access to this information is very important. We can only assist with crashes and other issues if we have enough information; the log file often provides that.

A typical scenario is when something in a structure causes Olex2 to crash. This happens sometimes, and we need to learn about these cases.

After a structure has crashed Olex2, it won't automatically load it on the next start-up because there is a chance that it will crash again, and then there will be no way out.

When that happens, Olex2 usually starts, but it will print the following:

>OLEX2 It appears that Olex2 has crashed last time: skip loading of the last file.

This allows you to manually examine the file and fix any problems.

In this state, it should be possible to load and refine the built-in sucrose example file and continue normally.

## The log file

The easiest way to find out all the required information is to type `log`, and you will find something like this in the first line:

>OLEX2 Olex2-1.5-dev 2023.04.26 svn.r751481ca MSC:192930146 on WIN64, Python: 3.8.6, wxWidgets: 3.0.4 for OlexSys

This not only tells us about the version of Olex2 but also about your operating system.

Sometimes, Olex2 can get into such a bad state that typing the `log` command has no effect.

In that case, please type

```
setvar defeditor notepad
log
```

If this __still__ doesn't work, you can access the log file, but this is trickier. The log files are stored in the location that is usually accessible from Olex2 with the command `shell DataDir()` -- but chances are that this won't work either if `log` doesn't work.

By default, this location is, for Windows, in this location:

>OLEX2 C:\Users\{name}\AppData\Roaming\Olex2Data

This folder will contain at least one folder with a long random name, such as this  _c7e46afa910e703cdb9ac0565d8f090d_. Each version of Olex2 will have its own folder. If you sort these folders by 'Date Modified,' you can identify the folder that contains information about the latest crash.

Inside this folder will be a folder named 'log' -- and the newest folder may contain information regarding a recent crash.

When contacting us with any issues, please include that log file in your query.




