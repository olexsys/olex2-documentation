---
title: "Atom Selection does not work as expected"
linkTitle: "unexpected_selection"
date: 2024-05-09
weight: 8 
description: With some graphics cards and drivers, clicking on an atom will select a **different** atom.
categories:
 - Bug
 - Graphics
tags:
 - selection
 - graphics card
 - problem
 - bug
 - openGL
 - options
---

For some people, clicking on an atom in Olex2 will select a _different_ atom instead of the intended one. This may happen only sometimes and under non-specific conditions.

The cause for this is a problem with the graphics card and its drivers.

Most of the reports we've got occured with Intel Graphics cards.

Sometimes, updating the graphics drivers may work.

If this doesn't help, you can set a special option in Olex2 by typing

```
options
```

straight into Olex2. A file will open (which is most probably empty). Just copy the following line into this file and then save it:

```
gl_selection=false
```

This will hopefully fix the problem.

For more options see also [Options](/olex2/docs/getting-started/settings/options)