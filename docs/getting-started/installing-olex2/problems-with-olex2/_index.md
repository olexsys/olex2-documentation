---
title: "Problems with running Olex2: FAQ"
linkTitle: "Problems"
date: 2023-06-29
weight: 10
description: Things sometimes go wrong. Here is a collection of frequently occuring problems.
categories:
 - Installation
tags:
 - Bugs
 - Problems
 - Not working
---

Olex2 should run smoothly on your chosen platform. But sometimes things go wrong, and there can be many reasons for this. We would like you to tell us about these issues -- and here are few tips and tricks how you can help is diagnose a problem.
