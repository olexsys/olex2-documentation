---
title: "Settings & Customisation"
linkTitle: "Settings"
date: 2021-04-28
weight: 20
type: docs
categories:
 - Installation
 
tags:
 - System Path
 - Crystallographic Software
---

Olex2 will generally work out of the box as it is installed. It has its own structure solution program (olex2.solve) and its own refinement program (olex2.refine).

In addition to the on-board tools, Olex2 recognises various other crystallographic software.

Olex2 can only find these, if the executables (the .exe files in MS Windows) are on the System Path. If you know what that means, then you probably know how to add programs to it.

Alternatively, you can add any number of folders that contain external software (ShelX, PLATON, SIR, Superflip, EDMA etc) to Olex2, and it will look into these locations for any known software.

{{< webp image="/images/settings_path.png" alt="Looking here for crystallographic programs">}}

Navigate to `@Home > Settings`, and you will find a box into which you can put the location of a folder that contains all your crystallographic software. If you want to add more than one folder, just type a semicolon (;) and then the next path. As you can see from the screen-shot, we recommend using s generally accessible and synchronised folder -- like a folder in a Dropbox account -- so that you can be sure to use the same crystallographic software for all your work.
