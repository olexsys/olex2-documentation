---
title: "Keyboard Shortcuts"
linkTitle: "Keyboard"
weight: 8
description: "Some keyboard shortcuts in Olex2"
---
 

There are plenty of pre-defined keystrokes and these can be very helpful. 

|Key Stroke    |  Action|
|----------    | ------- |
|**F2**        | Toggles the solid background colour.
|**F3**        | Toggles atom labels.
|**F4**        | Toggles the gradient background colour.
|**F5**        | Activates the Work tab.
|**F6**        | Activates the View tab.
|**F7**        | Activates the Tools tab.
|**F8**        | Activates the Info tab.
|**F_11**       | Toggles full screen mode on/off.
|**SHIFT+F_11** | Toggles GUI panel on/off. 
|**ESC**       | Deselects selected atoms or quits the current MODE of operation (such as atom labelling, grow etc).
|**DELETE**    | Deletes selected atoms or Q-peaks.
|**CTRL+<kbd>A</kbd>**    | Select everything.
|**CTRL+<kbd>G</kbd>**    | Enters the grow mode.
|**CTRL+<kbd>H</kbd>**    | Toggles the H atoms through show H / show H with H- bonding interactions / hide H.
|**CTRL+<kbd>I</kbd>**    | Inverts the selection. If nothing is selected works as ctrl+A
|**CTRL+<kbd>M</kbd>**    | Toggles the electron density maps as per Tools>Maps
|**CTRL+<kbd>O</kbd>**    | Launches the Open File dialog.
|**CTRL+<kbd>Q</kbd>**    | Cycles through no Q-peaks /show isolated Q-peaks /show connected Q-peaks.
|**CTRL+<kbd>R</kbd>**    | Runs a refinement.
|**CTRL+<kbd>T</kbd>**    | Cycles through hide molecule and show text /hide text/show molecule /show text and molecule.
|**CTRL+<kbd>U</kbd>**    | Deselects all of the current selection.
|**CTRL+<kbd>Z</kbd>**    | Undo the last action. It works whenever possible.
|**ALT+<kbd><--</kbd>**    | Reselect your last selection.
|**<kbd>TAB</kbd>**       | Expands the command (when typing in OLEX's console).
|**UP**    | Key brings the previously issued commands from the console.

------------------------------------------------------------------------
  