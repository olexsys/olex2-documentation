---
title: "Errors during the wavefunction calculation"
linkTitle: "Err: Wavefunction"
date: 25-07-2022
weight: 20
description: "What to do in case of errors during the wavefunction calculation"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Wavefunction calculations
---

In some cases, it happens, that the wavefunction calculation might not finalize or converge. In those cases there are a few things that one can try to solve the issue:

The general best practice would be to look at the log file of the wavefunction calculations. Did the calculation abort with an error message? If so, try understanding what might cause it! The logs are usually to be found in the structure **folder/olex/Wfn_Job**
In the case of **ORCA**, one common message for incomplete/unsuccessful installation reads: "Error during GTOint". Then either a piece of the ORCA installation is missing or MPI was not properly found, because in the calculations performed during NoSpherA2 the *GTOint* routine is the first parallel routine being called. That is why the beginning starts and afterwards it fails. Check that **MPI** and **ORCA** are completely installed and that ORCA is installed in a folder that has no **spaces** or **special characters** inside _(anything that is not a-z letters, underscore, minus, plus or numbers should be avoided)_

If the calculation did not converge try to change the inputs: Did you maybe assume a wrong spin state or wrong charge? (Check charge and multiplicity!) If your system does not converge on a high level immediately, try using a smaller basis set first (3-21G already goes quite a long way for starters!)

Check that disorder is handled correctly by visualizing the individual parts using the `@Disorder Tools` in `@Toolbox Work`.

In the case of **Tonto**: Try considering a lower convergence threshold or moving to ORCA/pySCF. If you have problems with that feel free to contact  [Florian K](/about/) from our team!