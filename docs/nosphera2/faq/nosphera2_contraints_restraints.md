---
title: "Constraints & Restraints"
linkTitle: "Constraints & Restraints"
date: 06-07-2021
weight: 19
description: "Can I use Constraints and Restraints with NoSPherA2?"
author: Horst Puschmann
categories:
 - NoSpherA2
tags:
 - constraints
 - restraints
---

Yes. You can use any and all standard tools when modelling a structure -- with or without NoSpherA2. All restraints and constraints that are supported during 'normal' refinement with olex2.refine are also supported when using non-spherical form factors. Remember: except for *not* using ancient spherical form factors, we are using sophisticated custom-made form factors, but otherwise everything stays the same and we use the same number of parameters.

## Constraints

The most common constraint you will need to use when refining with NoSpherA2 concerns the way hydrogen atoms are treated. In many cases -- and especially when the underlying data is of high quality -- all hydrogen atoms can be refined freely. But sometimes they can not, especially in disordered structures.

We suggest that you refine those hydrogen atoms freely that you *can* refine freely, and use the riding constraint for those that you can not refine freely.
Extremely usefull in this context:
-_RefineHDist_ (Keep (selected) hydrogen atoms in geometry according to their IAM AFIX instruction, but refine the distane freely)
-_NeutronHDist_ (change the distance for Hydrogen atoms to values proposed by Allen and Bruno in Acta Cryst. 2010, B66, 380-386.)

