---
title: "Localised Calculations"
linkTitle: "Local Calc's"
date: 09-11-2020
weight: 19
description: "Can I perform local QM calculations?"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Basis Sets
---

The nature of NoSpehrA2 is to use a _molecular wavefunction_ to calculate the atomic shape of each atom in the molecule. This is the reason why it is not possible to have a localised NoSpherA2 calculation.

But this would, anyway, make little sense since the diffraction experiment is performed on the *whole molecule*. The diffraction pattern is a Fourier transform of the electron density in the crystal, which means that certain effects in the density can not be purely assigned to any *one* atom. In other words: if you describe your organic ligand in a better way this will also improve the calculated uncertainties for your metal-hydride, since the overall agreement between model and experiment improves.

Things like the OSF, the phases assigned to reflections, and Fourier truncation ripples are affected by the complete model.

This being said, it is usually feasible to perform calculations for metal organic and organometallics on a normal laptop. If you choose a basis set like jorge-DZP (without DKH) you can even try to use a non-relativistic Hamiltonian (while using a relativistic one would require the -DKH suffix). If you don't want to spend a lot of time calculating the wavefunctions I can highly recommend the PBE (without 0) DFT-functional, or it's successord R2SCAN, which are very fast and still gives robust results.

Depending on your data quality and resolution, the accurate refinement of Ru-H bonds should be possible. Keep in mind that Ru has a ~40 times stronger signal than H, which is why you will need an excellent Rint and I/sigma, even at a higher resolution [by that I mean around 0.6-0.5 A in d-spacing] to get the best results.