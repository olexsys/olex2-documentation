---
title: "Errors during partitioning"
linkTitle: "Err: Partitioning"
date: 24-07-2022
weight: 20
description: "What to do in case of errors during the partitioning (NoSpherA2-Output)"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
---

In some cases it happens, that the partitioning fails and Olex2 prints an error message

>ERR Error: NoSpherA2-Output incomplete.

This happens if there was no successful partitioning and writing of the results.

Try checking if the wavefunction file is where it is expected from the NoSpherA2.log file in the folder of your structure. (.wfn, .wfx, .gbw, .fchk, etc. are valid file types) Make sure they are not empty but have a valid file size! Also try to check the file in **olex2/Wfn_Job/name.hkl** is valid and complete!

Another issue might be, that the creation of a .cif failed, before the calculation. Try running without NoSpherA2 (using olex2.refine) and see whether the .cif is updated accordingly or whether there are other issues producing errors in the Olex2 log (`log`)

Try following at what stage the calculation stops. There will be a **NoSpherA2.log** file in the main folder of the structure (`dire`).

The general sequence is

- Reading Wavefunction(s)
- Reading CIF(s)
- Reading hkl file
- Prune unrequired reflections
- Create atomic grids
- Compute densities
- Calculate scattering factors

Usually, once the stage of **atomic grids** is reached there are no more problems. If so, it might be a bug and it would be greatly appreciated if you could provide [Florian K](/about/) with the data so we can fix it as soon as possible!
