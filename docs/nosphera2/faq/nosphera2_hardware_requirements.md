---
title: "Hardware Requirements"
linkTitle: "Hardware Requirements"
date: 21-12-2022
weight: 19
description: "What are recommended specifications to run NoSpherA2?"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - Quantum Crystallography
 - HAR
 - FAQ
 - Hardware
---

In general NoSpherA2 attempts to be as efficiently operational as possible. 
It is aimed to be operational even on a laptop or office desk computer.

The most erssource hungry bit is the QM calculation. Using ORCA usually does a very good job at managing ressources. The Memory dedicated in the NoSpherA2-GUI is automatically determined to be approximately 2/3 of what is currently free on your computer.
If that is insufficient ORCA will automatically use different algorithms that use less memory but are slower. So even if you do not have the ressources to run efficiently you shoudl be able to run most calculations. Only for very large structrues you might run into a problem.

Currently there is no implementation of NoSpherA2 on GPUs, since the bandwith of the multiprocessors are not sufficient to handle the MOs required for the calcualtion of scattering factors. ORCA itself is CPU-only code.
A good multi-core CPU, for example with 16 cores and 128 GB RAM should be able to deal with almost any problem. Ideally the setup should aim at more than 4 GB of ram per thread you want to use. (keep in mind the OS needs some RAM, as well)
The refinement itself will make use of your CPUs capabilities quite well. So giving ORCA the CPUs it needs should get you the best performance on your refinements. Keep in mind that hyperthreading will not drastically improve the situation, since the additional cores require additional memory oeprations, limiting the effectiveness of the other cores, especially when thermal decrease of clock speeds happen.

These comments are just recommendations! Please feel free to share your experience with us at our helpdesk if you observe some bottleneck or have a very efficient setup you want us to include on this site.