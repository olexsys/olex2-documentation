---
title: "NoSpherA2"
linkTitle: "NoSpherA2"
weight: 8
type: docs
description: "Atoms are not spheres! Non-Spherical Atoms in Olex2."
categories:
 - NoSpherA2
tags:
 - HAR
 - refinement
 - non-spherical
 - form factor
---
 
{{< webp image="/images/diagram_kreis_gui.jpg"  width="600" alt="The basic workflow of Hirshfeld Atom Refinement (HAR) in NoSpherA2">}}


It is now possible to refine routine structures with olex2.refine using non-spherical atomic form factors. This technique is state-of-the art and will improve your structures significantly.

NoSpherA2 constitutes a real game change in the way routine structures will be done in the future -- and it is here, right now, for you to try.

The mathematical, theoretical and practical details of this technique are necessarily quite complex -- but that doesn't mean that it is difficult to use NoSpherA2! If you want to dig deeper we refer you to the original paper at DOI: 10.1039/D0SC05526C.

**If you use NoSpherA2 you agree to cite the following literature in your main manuscript.**

- Kleemiss et al., Chem. Sci., 2021, DOI: 10.1039/D0SC05526C
&nbsp; URL[https://pubs.rsc.org/en/content/articlepdf/2021/sc/d0sc05526c,PAPER]

NoSpherA2 is included with all versions of Olex2-1.3 -- in up-to date versions, it is activated by default -- ready to use! -- if your version of Olex2 isn't up to date, then please make sure you have automatic updating switched on and get the new version.

Florian Kleemiss gave a lecture on NoSpherA2 at the **Rigaky Advanced Crystallography School** on 11/12/2020. This video is well worth watching in full if you are interested in this subject!

{{< youtube id="U1-PTKViu4U" >}}

This is an older recording (March 2020) of a presentation by Florian Kleemiss, the principal author of NoSpherA2, explains what this is all about. It is about an hour long, but will give you a good insight into what NoSpherA2 is and what it isn't!

{{< youtube id="Q_Xt4jvwA8Q" >}}


