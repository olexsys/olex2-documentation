
---
title: "Fractional Dimension Plots"
linkTitle: "Fractional Dimension"
weight: 10
type: docs
date: 2017-01-05
description: >
  These plots visualize the residual electron density on both sides: +ve and -ve.
---



