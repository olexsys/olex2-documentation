
---
title: "Diagnostics Plots"
linkTitle: "Plots"
weight: 10
type: docs
date: 2017-01-05
description: >
  A visual representation of information related to your data and your model can really help your understanding of what is going on.
---



