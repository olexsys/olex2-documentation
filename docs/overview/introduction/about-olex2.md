---
title: "Introduction"
linkTitle: "Introduction"
weight: 2
description: "Introduction to Olex2"

categories:
 - Installation
tags:
 - Olex2 Versions
 - Updates
---
 
## Installation of Olex2

Olex2 is available free of charge for Windows, Mac OS X and Linux and you can download the installers and zip archives [here](/olex2/docs/getting-started/installing-olex2/). The Windows installer is a small application which downloads the appropriate Olex2 installation files for your system. In most cases, the installation will be very straightforward, but various manual options available -- including compiling your own version from source code repositories. For Mac OS X there is a DMG image for the release version and zip files for alpha and beta releases. Currently, we only provide 32-bit builds for Mac. For Linux, there are zip files for 32 and 64-bit versions of the operating system.

For a standard installation, Olex2 must be installed and launched at least once in an administrator mode for all of the features to function. Alternatively, the software can also be installed in a custom location, for example, your home directory.

## Olex2 Versions

There is always one official release version of Olex2. This is the version that you probably want to install -- and using the standard installation methods in their default mode, this is the version you will automatically get. 
For more information about the different Olex2 versions see [Olex2 versions](/olex2/docs/getting-started/versions/).

## Updating Olex2

Unless you change the settings in the menu bar (Help > Update Options), Olex2 will automatically check for updates on start-up if you are connected to the internet. If updates are found, these will be downloaded in the background and applied on the next start-up of the software. Updates only happen within the same series and variant (see above) -- eventually the current 1.3.0 version of Olex2 will update to the 1.3.N version, but it will not automatically update to Olex2 1.5 or higher -- instead a notice regarding the new release will appear.
