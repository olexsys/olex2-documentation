---
title: "Scripting Olex2"
linkTitle: "Scripting"
weight: 2
description: "There are two different levels on which Olex2 can be scripted"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - macro
---
 

The functionality of Olex2 can be extended on two levels: though macros and through python scripts.


## Olex2 Macros
There is a simple macro language, where you can group common tasks (and also redefine commands!). The built-in macros will be extended (and replaced!) with those macros you will find in the file you will see when you type `CODE emf`.

This will open `CODE DataDir()/custom.xld` file which will have some default macros (note that the location is the Olex2 installation dependent and will be cleared if you uninstall it choosing to delete user data).
You can add these lines at the end of the file:

```
<hello help="Prints current file name"
  <body
    <args>
    <cmd
      <1 "echo strcat('Hello, current file is ',filename()) -m=warning">
    >
  >
>
```
As you can see this is an XML-like file format (entries could be closed with either '>' or '/>'), you repeat commands to be executed one after another like

```
<1 "echo strcat('Hello, current file is ',filename()) -m=warning">
<2 "echo DataDir()">
```

You can find more exampled in macro.xld file in the Olex2 installation dir. At the end of that file you can find an important entry:

```
<user_onstartup help="dummy definition"
  <body
  
  >
>
```
This defines an empty 'user_onstartup' macro that will allow you to 'inject' something into Olex2 when its starts and will help us with Python development.

## Python Scripts
You can also script Olex2 using Python. All functions of Olex2 can be accessed and highly complex functionality can be added.

Considering that your scripts are in d:\tmp\name (note slashes in the macro).

```
<user_onstartup  help="Initialises Hunter's code"
  <body
    <args>
      <cmd
        <1 "py.run d:/tmp/hunter/init.py">
      >
  >
>
```
Then create a file `CODE d:\tmp\name\init.py` with the following content:

```
#################################################
import olex

def test():
  input_file_name = olex.f('fileOpen("Choose input HKL file", "HKL files|*.hkl",filepath())')
  print("You have selected %s" %input_file_name)

olex.registerFunction(test, False, 'hunter')
#################################################
```

After you start/restart Olex2 - there will be spy.hunter.test function available on the command line (start typing spy.h then tab then u then . etc to use autocomplete). When you run it - it will show file open dialog and print the selection in the console.
You can go one step further and make a shortcut for this function in macros:

```
<my_function  help="Runs my finction"
  <body
    <args>
      <cmd
        <1 " spy.name.test  ">
      >
  >
>
```
This will make `CODE spy.name.test` accessible through my_function.

