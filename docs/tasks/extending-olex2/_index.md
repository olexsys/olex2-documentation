---
title: "Scripting Olex2"
linkTitle: "Scripting"
weight: 2
description: "There are two different levels on which Olex2 can be scripted"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - macro
---
 

The functionality of Olex2 can be extended on two levels: though macros and through python scripts.
  