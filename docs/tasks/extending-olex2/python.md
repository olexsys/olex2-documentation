---
title: "Olex2 Programming with Python"
linkTitle: "Python"
weight: 2
description: "Writing Python code for Olex2"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - python
---

You can also script Olex2 using Python. All functions of Olex2 can be accessed and highly complex functionality can be added.

