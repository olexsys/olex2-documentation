---
title: "Olex2 Macros"
linkTitle: "Macros"
weight: 2
description: "Extending Olex2 through Macros"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - macro
---

There is a simple macro language, where you can group common tasks (and also redefine commands!). The built-in macros will be extended (and replaced!) with those macros you will find in the file you will see when you type `CODE emf`.

