---
title: "Olex2 Plug-Ins"
linkTitle: "Plug-In"
weight: 2
description: "Extending Olex2 through Plug-Ins"
categories:
 - Interface
tags:
 - scripting
 - programming
 - batch
 - plugin
 - macro
---


## Creating Extension Modules

Extending the functionality of Olex2 with python scripts is maybe bewildering at first, but once you got your first scripts going you will have access to the full structure model contained in Olex2 as well as the power of both of the crystallographic engines present: the Olex2 internal libraries as well as the cctbx. Everything you need to perform even the most complex crystallographic tasks will be available from within your scripts, and you get the benefit of a fully functional GUI thrown in for free.

We have not tested the module creation script on Linux -- and there are are some capitalisation related issues. We will fix these soon!

Creating your first Extension Module
In the Olex2 console (i.e. the main window), type the following line:

`CODE spy.pt.make_new_plugin(test)`

Will create the bare-bones of a new extension module in this location:

`CODE shell basedir()/util/pyUtil/PluginLib/plugin-test`

After a restart of Olex2, the GUI for your module will appear in the Tools tab of Olex2 (Make sure a structure is loaded, otherwise you won't see the Tools tab!). You have just created your first Olex2 extension module!

The Structure of Extension Modules
The folder plugin-test will contain three files:

 - test.htm -- The GUI
 - test.phil -- Settings File
 - test.py -- The Script

If you have already run your module, there will also be fourth file, the compiled python test.pyd, which you can safely ignore.

### test.htm
The GUI of Olex2 is largely defined by html -- slightly modified, but still fully recognisable as html. This file contains the outline of how it works to get something on to the Olex2 GUI. There's not much here, and I will modify our module generator to pre-fill this file with examples of real-world code soon.

### test.phil
This settings file contains variables that can be accessed from anywhere within Olex2 and also some basic settings regarding your module.

### test.py
This file contains the class test and the obligatory `__init__` function. This is really absolutely bare-bones at the moment, and I will update this soon with a few useful examples as to how to achieve various things in Olex2 scripting.

Here's an example for how the relevant part of this file could look like. Typing spy.test.f_to_e() from the Olex2 console will convert the F values from the loaded file to E values and write those to a file (which will be opened automatically).

{{< highlight py "linenos=inline, linenostart=199" >}}
class test(PT): 
  
def __init__(self): 
    super(test, self).__init__() 
    self.p_name = p_name 
    self.p_path = p_path 
    self.p_scope = p_scope 
    self.p_htm = p_htm 
    self.p_img = p_img 
    self.deal_with_phil(operation='read') 
    self.print_version_date() 
    self.setup_gui() 
    OV.registerFunction(self.f_to_e,True,'test') 
  
def f_to_e(self): 
    from cctbx import miller 
    formula = {} 
    for element in str(olx.xf.GetFormula('list')).split(','): 
      element_type, n = element.split(':') 
      formula.setdefault(element_type, float(n)) 
        
from cctbx_olex_adapter import OlexCctbxAdapter 
    oca = OlexCctbxAdapter() 
    E = miller.normalised_amplitudes(oca.reflections.f_obs, formula, None).array() 
    p = os.sep.join([OV.FilePath(), "fred.txt"]) 
    wFile = open (p,'w') 
    for line in E: 
      t = "%4s %4s %4s %7s\n" %(line[0][0], line[0][1], line[0][2], "%.6f" %line[1]) 
      wFile.write(t) 
    wFile.close() 
    olx.Shell(p) 
test_instance = test() 

print "OK."
{{< / highlight >}}
