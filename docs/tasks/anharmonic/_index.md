---
title: "Anharmonic Refinement"
linkTitle: "Anharmonic ADPs"
weight: 8
description: "Refining atoms anharmonically"
categories:
 - Crystallography
tags:
 - refinement
 - anharmonic
---

It is possible to refine the displacements of selected atoms anharmonically in olex2.refine. Here's how it is done.

{{< youtube id="UYLcT3nXO7g" >}}

**Warning**: This is an experimental feature -- it works, but please use it at your own disgression. You must be aware that this technique is open to abuse.

Select the atom(s) you want to refine anharmonically and type `anis -a`. If you don't want to refine atoms anharmonically any longer, please select these atoms (or all!) and just type `anis` -- this will remove the anharmonic flag for all.

You can visualise the resulting 'shape' in terms of mean square displacement of the atoms by switching on an alternative display style for **all** atoms, type `MSDSView -a=anh -s=1.5` Please keep in mind that the scale of 1.5 will apply and that for the anharmonic atoms only the contribution of the anharmonic terms is shown. You can disable this view by typing `kill MSDS`.

A judgement whether it might be reasonable to refine anharmonic motion can be made according to Kuhs' rule: Kuhs *Aust. J. Phys.* **1988**,41,369-82. 
0.074758 sqrt(n/U^2) might give an estimate what resolution in sinus theta over lambda is required to have a significant difference between anharmonic and harmonic motion.