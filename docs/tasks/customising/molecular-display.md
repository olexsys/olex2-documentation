---
title: "Styles & Scenes"
linkTitle: "Styles & Scenes"
weight: 4
description: "How to manage visual styles and scenes"
categories:
tags:
 - drawing
 - style
 - atom display
---


Visual styles can be modified and saved for later use in Olex2.
