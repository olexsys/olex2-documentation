---
title: "Refining Electron Diffraction Structures"
linkTitle: "ED Structures"
weight: 8
type: docs
description: "Refining structres based on electron diffraction data"
categories:
 - Electron Diffraction
tags:
 - refinement
 - SFAC
---

It is possible to use Olex2 to work with structures based on electron diffraction data. In general, there are not many differences in using Olex2 for ED structures.

### Loading ED data

ShelXL and olex2.refine are both (at the moment) assuming [kinematic diffraction](http://pd.chem.ucl.ac.uk/pdnn/diff2/kinemat1.htm). This is obviously a big issue in ED, where dynamic effects are huge.

This means: you will start with the standard .hkl and .ins files and proceed as normal. These files will result from your standard ED data processing routines performed by the various integration programs.

If no SFAC lines are present in the .ins file, then Olex2 will insert the appropriate lines automatically. It will not replace any existing information.

>OLEX2 The SFAC information will be taken from the table that is present in BaseDir()/util/ED/SF.txt@. If you wish to load different tables, you can put SF.txt into DataDir()/ED@ folder or type the following line in Olex2: `CODE spy.sfac.generate_ED_SFAC path/to/file/sf_name.txt`.


### Solving ED structures

There is really no difference here -- all structure solution programs may well work for ED data, but you must keep in mind that the data may be incomplete. It may be harder to actually get to a structure solution!

ShelXT is probably the structure solution program of choice, since it will give you a very good guess at the correct space group for free!

### Refining ED Structures

ED structures can be refined just like structures based on X-ray diffraction data. But there are differences:

  - Using the kinematic approximation, the $R$ factors will always be large. An $R1$ of ~ 12-15% is excellent.
  - The difference in scattering power of elements is not always intuitive: C, N, and O have an almost identical scattering power and can not reliably be told apart
  - EXTI always has to be refined -- and it will take absolutely huge values!


### EXTI

ShelXL and olex2.refine both refine the extinction parameter $x$ by least-squares, where $F_c$ is multiplied by:

>EQ $$k \times \left[ 1 + \frac{0.001 \times x \times F_c^2 \times \lambda^3 }{ sin(2\theta) }\right]^{-1/4}$$


where $k$ is the overall scale factor. Note, the wavelength $\lambda$ is in this equation as the cube! This is the reason why the EXTI parameters refine to very large values.
