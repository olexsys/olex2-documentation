---
title: "Using Maps and Masks"
linkTitle: "Using Maps and Masks"
weight: 8
description: "Electron density maps and Solvent Masks"
categories:
 - Analysis
tags:
 - electron density
 - residual density
 - maps
 - solvent masking
 - refinement
---
 
## Maps \index{maps}
Various maps can be calculated by Olex2 and are displayed interactively. These options are available under `@Tools|Maps`:
The appearance of the maps can be changed under the top heading of @@View@@ (e.g. plane, contour, contour + plane, surface, wire, points). The additional options to adjust the view change depending on which option has been selected. The tool information provides more detail on the different views.

### Calculate Voids \index{voids}
This calculates all voids in the structure. The resolution of the map (Res/ANGST) and the distance from atoms (Distance/ANGST) can be adjusted to user-defined values. Click on @@Void@@ to toggle between displaying and not displaying voids. The @@Precise@@ option uses a different algorithm which will return more precise values but the calculation will take much longer. The approximate values are normally all you need.

### Calculate Solvent Accessible Voids
Calculates voids that are large enough to contain a solvent. Probe/ANGST adjusts the probe size (think of it as a sphere rolling about the structure - a smaller sphere will fit into smaller gaps and therefore return a larger void than a larger sphere would). The Grid /ANGST is the resolution of the map that will be explored when calculating the voids. If the resolution is too high, the calculation might take a Very Long Time at not much benefit. Click on @@Void@@ to toggle between displaying and not displaying solvent accessible voids. On the graphics screen the size of any solvent accessible voids that are found will be displayed.

### Electron Density \index{electron density map}
In order to see whether disorder is likely it can be very useful to see a map of the residual electron density.

The resolution of the map can adjusted using Res /ANGST, the smaller the number the more detailed the map. The @@Mask@@ tick-box enables the map to either overlay on the structure (box ticked) or lie to one side of the structure (box unticked). There are five different map options available: diff, Fc, 2Fo-Fc, Fo and Deformation (Fc-Fmc). The latter only makes sense using NoSpherA2, since it is a difference between the spherical Atom model and the current model.

An electron density map can also be calculated quickly under `@Work|Toolbox-Work|Electron-Density-Map` which will use the settings selected under `@Tools|Maps|Electron-Density`. If refinement has been attempted using both ShelXL and olex2.refine, there is the option to select either *olex* (Olex2 will calculate the map itself) or .fcf (the saved structure factors will be used). Ensure that the file from the last cycle of refinement is used.

For the plotting of Maps from the .fcf file it is required that the file contains the Phases of the model in some manner. LIST 6 and 3 are possible candidates for this, since otherwise it is not possible to recalculate the density. ShelXL will try to remove the anomalous dispersion signal from the structure factors and therefore only print the merged values for Friedel equivalents. olex2.refine will print the values *without* removing the dispersion and therefore a merging of Friedel equivalents is not possible. The residual density maps from these files will differ!

When comparing the value of Q-Peaks reported by either refinement program it is also important to keep in mind that the grids used during the refinement might differ from that used during the plotting of residual density maps. Therefore the values might and most likely will differ, sometimes significantly, for example when the gridding in the case of Q-peak searches missed the actual peak by a few sub-\ANGST. It is strongly recommended to look at the maps with higher resolutions after refinement to not be a victim of these issues.

### Masks \index{masks}
The @@Masks@@ option serves as an alternative to SQUEEZE which is implemented in Platon (*http://www.cryst.chem.uu.nl/platon/*). These sorts of approaches should only be used when the solvent cannot be identified or modelled, although every effort should be made to try and do this. 

## Modelling Solvents
Where possible, every attempt should be made to model the solvent if it can be identified and sensibly modelled, as this will give the most accurate structure and therefore calculated structure factors. However, this is not always possible if the structure contains large voids enabling multiple solvent molecules to be incorporated at various positions within the structure. In such cases, masks (either calculated by Olex2 or by Platon/SQUEEZE \index{Platon} can be used.
Often a solvent is disordered over more than one position and needs to be modelled as disordered.
A solvent is not always present is every ASU \index{ASU}. For example, sometimes the solvent is only loosely bound in the structure and so it may evaporate out i.e. the occupancy does not necessarily equal 1. Care has to be taken when refining the occupancy due to the link between occupancy and thermal parameters.
  