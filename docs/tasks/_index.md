---
title: "Tasks"
linkTitle: "Tasks"
weight: 6
type: docs
description: >
  Common tasks and how to go about them: Use-Case scenarios.
---

This is our "how-to" section. There is no sequence to the information presented here -- it's just a bunch of 'taks' that you may want to perform using Olex2.
