---
title: "Solvent Masking"
linkTitle: "Solvent Masking"
weight: 8
description: "When entities in the solvent sphere just can't be modelled."
categories:
 - Model Building
tags:
 - solvent masking
 - SQUEEZE
---
 
There are times when -- try as one might -- it is just not possible to model whatever is present in the solvent sphere. The question then becomes: what shall I do with this?

One could just ignore the issue altogether -- and do nothing. The problem is, that there will be a lot of unaccounted-for electron density left and that means the entire structure model will suffer. This is not _really_ an option.

In the distant past, people sometimes just added 'dummy' atoms: carbon, oxygen, helium -- anything -- and assigned or refined the occupancies of these things. This 'dealt' with the residual densities to some degree, but there would always be more peaks popping up -- and the resulting sum formula was, of course, incorrect.

In 1990, Ton Spek and P. van der Sluis published the [Bypass](http://scripts.iucr.org/cgi-bin/paper?S0108767389011189) paper, where they describe "an effective method for the refinement of crystal structures containing disordered solvent regions" -- as the title of the paper has it.