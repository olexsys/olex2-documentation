---
title: "Structure Determination of Sucrose"
linkTitle: "Sucrose"
weight: 2
type: docs
description: "Get into Olex2 with this example structure!"
---

{{< webp image="/images/sucrose_diagram.png" alt="Connectivity diagram of sucrose.">}}

This section provides a guide to the basic structure solution and refinement of sucrose. With these step-by-step instructions, you will be able to repeat this structure solution and refinement process for yourself, and this will help you become familiar with the way in which Olex2 works.

You can also watch our video on how to treat this structure -- this video is a little bit older but it might still be useful.

{{< youtube id="8QvmKSnJFPs" >}}  

