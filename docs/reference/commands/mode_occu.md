---
title: "mode occu"
weight: 9
description: Sets the occupancy for subsequently clicked atoms
categories:
 - Commands
 - Model Building
tags:
 - refinement
 - occupancy
---

>A occupancy_to_set

>B Sets atom occupancy to the provided value for subsequently clicked atoms.
