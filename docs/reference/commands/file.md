---
title: "file"
weight: 9
description: Saves the current model to varios file formats
categories:
 - Commands
tags:
 - interface
 - .cif
 - .ins
 - .xyz
---

>A file

>B Saves current model to a file. By default an ins file is saved and loaded. It is also possible to save the current file in cif,ins,p4p,mol,mol2,pdb,xyz formats.