---
title: "showp"
weight: 9
weight: 9
description: Show only the named parts
categories:
 - Commands
 - Interface
 - Essential
tags:
 - disorder
 - PART
---

>A [any]; space separated part number(s)

>B Shows only the parts requested: `CODE showp 0 1` will show parts 0 and 1, `CODE showp 0` just part 0. `CODE showp` by itself will display all parts.
