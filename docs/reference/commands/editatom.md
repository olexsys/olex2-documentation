---
title: "editatom"
weight: 9
description: Brings up an edit window where the atom properties can be edited in ShelX format
categories:
 - Commands
tags:
 - refinement
---

>A [r=2.7 ANGST] A1 or  selected atom [-h] [-q]

>B Prints a list of those atoms within a sphere of radius r around the specified atom. If more than one atom is selected, the one that was selected first is used.

>C
 * **-h**: adds hydrogen atoms to the list
 * **-q**: adds Q-peaks to the list
