---
title: "sgs"
weight: 9
description: Space Group Settings
categories:
 - Commands
 - Interface
tags:
 - space group operation
 - space group
---

>A SGS arguments [any except none]

>B Changes the current space group settings using provided cell setting (if applicable) and axis, or 9 transformation matrix elements and the space group symbol. If the transformed HKL file is required, it should be provided as the last argument (something like 'new.hkl')

>D `sgs 1 0 0 0 -1 0 0 0 -1 P-1 new.hkl`.

<!-- {{< youtube id="6alkt9OkRgo" >}} -->

<br>

<div id="som-player" class="som-embed-player" data-id="c3iYX5VZe4t"><script src="https://screencast-o-matic.com/player/appearance/c3iYX5VZe4t"></script><iframe width=100% height=100% style="border:0;" scrolling="no" src="https://screencast-o-matic.com/player/c3iYX5VZe4t?width=100%&height=100%&ff=1&title=0" allowfullscreen="true"></iframe></div>
