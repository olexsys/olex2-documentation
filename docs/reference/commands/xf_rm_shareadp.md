---
title: "xf.rn.ShareADP1"
weight: 9
description: Generate a shared, rotated ADP constraint
categories:
 - Commands
 - Interface
tags:
 - disorder
 - ADP
---


# xf.rm.ShareADP1

>A [atoms]

>B Generates the shared, rotated ADP constraint. For 3 atoms, it generates an ADP rotated around the bond e.g. around X-C bond in X-CF\low{3}. For more atoms, it creates an ADP rotated around the normal of the plane formed by the atoms.
