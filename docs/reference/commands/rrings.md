---
title: "rrings"
weight: 9
description: Find and restrain rings
categories:
 - Commands
tags:
 - disorder
 - restraint
---

>A [d=1.39] [esd=0.01] ring_content or selection

>B Finds rings using the selection or rings content (like C6). It also sets DFIX restraint for the bond lengths using the d parameter and FLAT with e.s.d. of 0.1 restraint for the ring. It also adds SADI restraints for the 1-3 distances. If d is negative, the SADI restraint is used instead.

Example: 6-membered ring with C1 to C5 and N1 is present, type command 'rrings NC5' will generate:
DFIX 1.39 0.0 C1 C2 C2 C3 C3 C4 C4 C5 C5 N1 N1 C1
SADI 0.02 C1 C2 C2 C3 C3 C4 C4 C5 C5 N1 N1 C1
FLAT 0.1 C1 C2 C3 C4 C5 N1