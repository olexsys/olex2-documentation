---
title: "zoom"
weight: 9
description: Get and Set the value of the current zoom level
categories:
 - Commands
 - Interface
tags:
 - select
---

>B To get the current value of the scene zoom use:
`CODE echo gl.GetZoom()`
To set current zoom to a certain value use:
`CODE gl.zoom(math.eval(Value-gl.GetZoom()))`
This can be used to put different structures on the same scale. Note that a value of 1 corresponds to the scale where the smallest dimension of the screen view is 1 Angstrom.
To reset zoom to the default value for the current model use:
`CODE gl.zoom`
