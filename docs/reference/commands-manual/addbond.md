---
title: "addbond"
linkTitle: "addbond"
weight: 9
description: Adds a bond to the connectivity list
categories:
 - Commands
 - Essential
 - Model Building
tags:
 - refinement
 - connectivity-table
---

>A A1, A2 or atoms

>B Adds a bond to the connectivity list for the specified atoms. This operation will also be successful if symmetry equivalent atoms are specified.

To remove a bond from the bond list, please use [delbond](/olex2/docs/reference/commands/delbond/)


