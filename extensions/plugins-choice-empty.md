---
title: "Plugins Choice Empty"
linkTitle: "Plugins Choice Empty"
weight: 10
description: "The drop-down box in Home > Extension Modules remains empty"
tags: ['troubleshooting', 'extensions']
status: draft
---

Available extensions are displayed from the drop-down box in Home > Extension Modules. If there is an empty choice like depicted in the figure here, then please copy and paste the following line into Olex2:
```
 @py "url="https://secure.olexsys.org/PluginProvider1/available?t=1.3-alpha"\nimport HttpTools\nprint(HttpTools.make_url_call(url, {}).read())"
```

>CRYST When you copy this string, make sure that no empty spaces will be present either at the beginning or the end of the line when you paste it into Olex2! The above text contains a 'stubborn' space _before_ the '@' symbol...

If you get a 'stack trace' ending in something like this, then we know that there is some sort of network problem.

```
raise urllib2.URLError(e)urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:726)>
```
