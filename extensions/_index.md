---
title: "Extension Modules"
linkTitle: "Extension Modules"
weight: 30
menu:
 olex2:
status: draft
description: "Extend the functionality of Olex2 with our Extension Modules"
---
 

# Olex2 Extension Modules
We offer three Extension Modules that work from within Olex2, listed below. These are available for a 3 month trial period after which we offer both personal and site licenses, so please feel free to [contact us](../../about/) for a quote.

## ReportPlus
This module allows the generation of highly customisable, quality structure reports, either for a single structure or for multiple structures. This is very useful when it comes to combining structural data from multiple structures to create a set of structures ready for publication.

## DrawPlus
This is a collection of quick drawing styles that allows the production of sophisticated structural drawings and packing diagrams. Hydrogen bonding networks and pipi bonded systems, for example, can be visualised in publication-quality drawings at the push of a button.

## 3DRendering
This module can create the input files that are required for 3D printers in various formats.