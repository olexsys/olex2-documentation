---
title: "3DPlus"
linkTitle: "3DPlus"
weight: 4
description: "A module to export structures in various 3D Printer formats"
categories:
 - interface
tags:
 - extensions
 - drawing
 - report
---

There is no GUI for this Olex2 extension. To export a structure in a 3D Printer format, you need to type the relevant command in the Olex2 command line:

  - ```pictSTL <name.stl>``` Exports STL 3D model
  - ```pictPLY <name.ply>``` Exports PLY 3D model
  - ```pictWRL <name.wrl>``` Exports VRML 3D model

These other formats might also be of interest	

  - ```pictPR <name.pov>``` POVRAY output
  - ```pictTEX <name.tex>``` Experimental TEX/PGF rendering
  
  
At the time of writing, these are the only available formats -- if you are looking for different formats, please let us know!
  
