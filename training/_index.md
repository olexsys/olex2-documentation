---
title: "Training"
linkTitle: "Training"
weight: 20
menu:
 olex2:
description: Crystallographic Training
---


{{< webp image="/images/workshop_01.jpg" alt="">}}


# Olex2 Workshops
We offer a variety of Olex2 workshops which can be tailored to suit your requirements. These can all be targeted at any level from undergraduate upwards and at complete beginners to more advanced users, so please [contact us](../../about/) to discuss your needs.



## Personalised Online Group Workshops
If you give us the areas that you are interested in covering and the target audience we can put together a suitable workshop. These workshops could be run as a one off or in 1 or 2 hour blocks over several weeks depending on your needs and can include time for participants to use Olex2 and ask questions as required. We require a minimum of 4 participant and suggest a maximum of 12 participants but can adjust to meet your needs. Pricing depends on number of participants, workshop length and frequency, so please [contact us](../../about/)  to discuss your requirements and get a quote. As an example, a one hour small group workshops of 6 participants would be GBP25 per person per hour.


## One-To-One Online Olex2 Consultations
These are designed for anyone from beginner level upwards and the content can be directed by you but could include an introduction to using Olex2 for the first time, an introduction to new or more complex feature or a discussion around a problematic datasets (obviously we can't promise to resolve the issues but will happily look and advise on the approaches we take with tricky data!). These are charged at GBP80/hour.

## General Workshops
We run general workshops on a variety of topics at intervals throughout the year, which are detailed on the webpages as they arise. If you would like to be included on a mailing list to be notified about upcoming General Workshops, please [contact us](../../about/)  link on the webpages. The content, length, price and maximum number of attendees for these workshops will be detailed on the webpage alongside the registration for the workshop.


## In-Person Workshops at your Site
We will put together a suitable workshop based on the areas that you would like covered and the target audience. These workshops are a minimum of 4 hours and we have generally found they run best including time for participants to use Olex2 and ask questions. If this is of interest please [contact us](../../about/)  to discuss your requirements and receive a quote.